# next-cypress-gitlab-ci-example

This is an example that runs a [Cypress](https://www.cypress.io/) test in Docker using a GitLab CI pipeline. It also uses the [GitLab Container Registry](https://docs.gitlab.com/ee/user/packages/container_registry/) for caching purposes.

## Usage

- push commit
- see cypress test run here: https://gitlab.com/saltycrane/next-cypress-gitlab-ci-example/-/pipelines
